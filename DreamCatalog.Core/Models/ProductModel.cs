﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DreamCatalog.Core.Models
{
    public class ProductModel : BaseModel
    {
        public string Name { get; set; }
        [Column(TypeName = "decimal(6,2)")] public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string PhotoPath { get; set; }
        public long TimeStamp { get; set; }

        public Guid CategoryId { get; set; }
        [ForeignKey(nameof(CategoryId))] public CategoryModel Category { get; set; }
    }
}