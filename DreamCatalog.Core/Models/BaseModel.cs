﻿using System;

namespace DreamCatalog.Core.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
    }
}