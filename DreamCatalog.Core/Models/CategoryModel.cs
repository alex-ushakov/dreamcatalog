﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DreamCatalog.Core.Models
{
    public class CategoryModel : BaseModel
    {
        public string Name { get; set; }

        public Guid? ParentId { get; set; }
        [ForeignKey(nameof(ParentId))] public CategoryModel Parent { get; set; }
        public List<CategoryModel> Children { get; set; }

        public List<ProductModel> Products { get; set; }
    }
}