﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DreamCatalog.Core.DTOs;
using DreamCatalog.Core.Helpers;
using DreamCatalog.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace DreamCatalog.Core.Repositories
{
    public class ProductsRepo : IProductsRepo
    {
        private readonly DreamCatalogContext _context;
        private readonly IMapper _mapper;

        public ProductsRepo(DreamCatalogContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<ProductGetList>> GetList(Guid categoryId, string sort = null, long? timestamp = null,
            int? offset = null,
            int? amount = null, CancellationToken token = default)
        {
            var queryable = _context.Products
                .OrderBy(model => model.Name)
                .Where(model => model.CategoryId == categoryId);

            if (timestamp != null)
                queryable = queryable.Where(model => model.TimeStamp < timestamp.Value);

            if (!string.IsNullOrWhiteSpace(sort))
                queryable = queryable.OrderBy(model => sort);

            if (offset != null && offset >= 0)
                queryable = queryable.Skip(offset.Value);

            if (amount != null && amount.Value > 0)
                queryable = queryable.Take(amount.Value);

            var result = _mapper.Map<List<ProductGetList>>(await queryable.ToListAsync(token));
            return result;
        }

        public async Task<ProductGet> Get(Guid id, CancellationToken token = default)
            => _mapper.Map<ProductGet>(await _context.Products.SingleAsync(model => model.Id == id, token));

        public async Task Add(ProductAdd product, CancellationToken token = default)
        {
            var entity = _mapper.Map<ProductModel>(product);
            entity.TimeStamp = HiResDateTime.UtcNowTicks;

            await _context.Products.AddAsync(entity, token);
            await _context.SaveChangesAsync(token);
        }

        public async Task Edit(Guid productId, ProductEdit product, CancellationToken token = default)
        {
            var entity = await _context.Products.SingleAsync(model => model.Id == productId, token);
            entity = _mapper.Map(product, entity);

            _context.Products.Update(entity);
            await _context.SaveChangesAsync(token);
        }

        public async Task Delete(Guid id, CancellationToken token = default)
        {
            var product = await _context.Products.SingleAsync(model => model.Id == id, token);
            _context.Remove(product);
            await _context.SaveChangesAsync(token);
        }

        public async Task<bool> IsExist(Guid id, CancellationToken token = default)
            => await _context.Products.SingleAsync(model => model.Id == id, token) != null;
    }
}