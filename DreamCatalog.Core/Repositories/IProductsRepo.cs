﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DreamCatalog.Core.DTOs;

namespace DreamCatalog.Core.Repositories
{
    public interface IProductsRepo
    {
        Task<List<ProductGetList>> GetList(Guid categoryId, string sort = null, long? timestamp = null,
            int? offset = null,
            int? amount = null, CancellationToken token = default);

        Task<ProductGet> Get(Guid id, CancellationToken token = default);
        Task Add(ProductAdd product, CancellationToken token = default);
        Task Edit(Guid productId, ProductEdit product, CancellationToken token = default);
        Task Delete(Guid id, CancellationToken token = default);
        Task<bool> IsExist(Guid id, CancellationToken token = default);
    }
}