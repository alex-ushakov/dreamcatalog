﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DreamCatalog.Core.DTOs;

namespace DreamCatalog.Core.Repositories
{
    public interface ICategoriesRepo
    {
        Task<List<CategoryGet>> GetTreeView(CancellationToken token = default);
        Task<bool> IsExist(Guid id, CancellationToken token = default);
    }
}