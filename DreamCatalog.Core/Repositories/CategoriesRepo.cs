﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DreamCatalog.Core.DTOs;
using Microsoft.EntityFrameworkCore;

namespace DreamCatalog.Core.Repositories
{
    public class CategoriesRepo : ICategoriesRepo
    {
        private readonly DreamCatalogContext _context;
        private readonly IMapper _mapper;

        public CategoriesRepo(DreamCatalogContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<CategoryGet>> GetTreeView(CancellationToken token = default)
        {
            var list = _mapper.Map<List<CategoryGet>>(await _context.Categories
                .Include(model => model.Products)
                .ToListAsync(token));

            return list
                .Where(model => model.Parent == null)
                .ToList();
        }

        public async Task<bool> IsExist(Guid id, CancellationToken token = default)
            => await _context.Categories.SingleAsync(model => model.Id == id, token) != null;
    }
}