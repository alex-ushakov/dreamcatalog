﻿using AutoMapper;
using DreamCatalog.Core.DTOs;
using DreamCatalog.Core.Models;

namespace DreamCatalog.Core
{
    public class AutoMapperSettings : Profile
    {
        public AutoMapperSettings()
        {
            CreateMap<CategoryModel, CategoryGet>().ReverseMap();

            CreateMap<ProductModel, ProductGetList>().ReverseMap();
            CreateMap<ProductModel, ProductGet>().ReverseMap();
            CreateMap<ProductAdd, ProductModel>().ReverseMap();
            CreateMap<ProductEdit, ProductModel>().ReverseMap();
        }
    }
}