﻿using System;
using System.Collections.Generic;

namespace DreamCatalog.Core.DTOs
{
    public class CategoryGet
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public CategoryGet Parent { get; set; }
        public List<CategoryGet> Children { get; set; }

        public List<ProductGetList> Products { get; set; }

        public class ProductGetList
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public int Quantity { get; set; }
            public string PhotoPath { get; set; }
            public long TimeStamp { get; set; }
        }
    }
}