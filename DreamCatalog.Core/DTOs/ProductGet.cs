﻿using System;

namespace DreamCatalog.Core.DTOs
{
    public class ProductGet
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string PhotoPath { get; set; }
        public long TimeStamp { get; set; }
    }
}