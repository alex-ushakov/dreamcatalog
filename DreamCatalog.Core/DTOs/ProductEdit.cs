﻿namespace DreamCatalog.Core.DTOs
{
    public class ProductEdit
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string PhotoPath { get; set; }
    }
}