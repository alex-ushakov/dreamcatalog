﻿using System;
using System.Threading;

namespace DreamCatalog.Core.Helpers
{
    public class HiResDateTime
    {
        private static long _lastTimeStamp = DateTime.UtcNow.Ticks;

        public static long UtcNowTicks
        {
            get
            {
                long original, newValue;
                do
                {
                    original = _lastTimeStamp;
                    var now = DateTime.UtcNow.Ticks;
                    newValue = Math.Max(now, original + 1);
                } while (Interlocked.CompareExchange
                             (ref _lastTimeStamp, newValue, original) != original);

                return newValue;
            }
        }
    }
}