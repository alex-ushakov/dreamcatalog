﻿using System;
using DreamCatalog.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace DreamCatalog.Core
{
    public sealed class DreamCatalogContext : DbContext
    {
        public DreamCatalogContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<CategoryModel> Categories { get; set; }
        public DbSet<ProductModel> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<CategoryModel>()
                .HasData(
                    new
                    {
                        Id = Guid.Parse("8e0a7e4d-da38-4d9c-9be9-99a3d517df20"),
                        Name = "Одежда, обувь, аксессуары"
                    },
                    new CategoryModel
                    {
                        Id = Guid.Parse("09c6981e-f02f-4f82-9ca3-c4e14e413055"),
                        Name = "Женская одежда",
                        ParentId = Guid.Parse("8e0a7e4d-da38-4d9c-9be9-99a3d517df20")
                    },
                    new
                    {
                        Id = Guid.Parse("d95de1d6-61a0-4a6d-aa3d-65cc2f70cda6"),
                        Name = "Верхняя",
                        ParentId = Guid.Parse("09c6981e-f02f-4f82-9ca3-c4e14e413055")
                    },
                    new
                    {
                        Id = Guid.Parse("cf8d9d49-a510-4745-9108-d0c1b19d274b"),
                        Name = "Плащи, тренчи",
                        ParentId = Guid.Parse("d95de1d6-61a0-4a6d-aa3d-65cc2f70cda6")
                    },
                    new
                    {
                        Id = Guid.Parse("7f717b70-4479-4cdf-8ec0-6da5981643a1"),
                        Name = "Одежда для спорта и танцев",
                        ParentId = Guid.Parse("09c6981e-f02f-4f82-9ca3-c4e14e413055")
                    },
                    new
                    {
                        Id = Guid.NewGuid(),
                        Name = "Беговая одежда",
                        ParentId = Guid.Parse("7f717b70-4479-4cdf-8ec0-6da5981643a1")
                    }
                );

            builder.Entity<ProductModel>()
                .HasData(
                    new ProductModel
                    {
                        Id = Guid.NewGuid(),
                        CategoryId = Guid.Parse("cf8d9d49-a510-4745-9108-d0c1b19d274b"),
                        Name = "Nike",
                        PhotoPath = "nike.png",
                        Quantity = 51,
                        Price = 7790.0m,
                        TimeStamp = DateTime.UtcNow.Ticks
                    },
                    new ProductModel
                    {
                        Id = Guid.NewGuid(),
                        CategoryId = Guid.Parse("cf8d9d49-a510-4745-9108-d0c1b19d274b"),
                        Name = "Lost Ink Petite",
                        PhotoPath = "da",
                        Quantity = 13,
                        Price = 3020.0m,
                        TimeStamp = DateTime.UtcNow.Ticks
                    });
        }
    }
}