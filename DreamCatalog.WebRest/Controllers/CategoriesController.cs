﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DreamCatalog.Core.Repositories;
using DreamCatalog.WebRest.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DreamCatalog.WebRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoriesRepo _categoriesRepo;
        private readonly IMapper _mapper;

        public CategoriesController(ICategoriesRepo categoriesRepo, IMapper mapper)
        {
            _categoriesRepo = categoriesRepo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<CategoryGetVM>>> GetAll(CancellationToken token)
            => _mapper.Map<List<CategoryGetVM>>(await _categoriesRepo.GetTreeView(token));
    }
}