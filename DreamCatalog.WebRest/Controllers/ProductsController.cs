﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DreamCatalog.Core.DTOs;
using DreamCatalog.Core.Repositories;
using DreamCatalog.WebRest.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DreamCatalog.WebRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ICategoriesRepo _categoriesRepo;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IProductsRepo _productsRepo;

        public ProductsController(ICategoriesRepo categoriesRepo, IProductsRepo productsRepo, IMapper mapper,
            ILoggerFactory loggerFactory)
        {
            _productsRepo = productsRepo;
            _mapper = mapper;
            _categoriesRepo = categoriesRepo;
            _logger = loggerFactory.CreateLogger(GetType());
        }

        [HttpGet("/categories/{categoryId}/products")]
        public async Task<ActionResult<List<ProductGetListVM>>> GetList(Guid categoryId, string sort = null,
            long? timestamp = null, int offset = 0, int amount = 20, CancellationToken token = default)
        {
            if (ModelState == null)
                BadRequest("The invalid model");

            if (offset < 0)
                ModelState.AddModelError(nameof(offset), "Offset can not be less than 0");

            if (amount < 10)
                ModelState.AddModelError(nameof(amount), "Amount can not be less than 10");

            if (amount > 25)
                ModelState.AddModelError(nameof(amount), "Amount can not be more than 25");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var dtos = await _productsRepo.GetList(categoryId, sort, timestamp, offset, amount, token);
            var result = _mapper.Map<List<ProductGetListVM>>(dtos);
            return result;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductGetVM>> Get(Guid id, CancellationToken token)
        {
            var isExist = await _productsRepo.IsExist(id, token);
            if (!isExist)
                NotFound();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return _mapper.Map<ProductGetVM>(await _productsRepo.Get(id, token));
        }

        [HttpPost("/categories/{categoryId}/products")]
        public async Task<ActionResult> Add(Guid categoryId, [FromForm] ProductAddVM addVM, IFormFile uploadedFile, CancellationToken token)
        {
            if (ModelState == null)
                BadRequest("The invalid model");

            if (!await _categoriesRepo.IsExist(categoryId, token))
                return NotFound();

            if (uploadedFile == null)
                ModelState.AddModelError("image", "Not uploaded image");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // TODO implement the logic of image loading

            var dto = _mapper.Map<ProductAdd>(addVM);
            dto.CategoryId = categoryId;

            try
            {
                await _productsRepo.Add(dto, token);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpPut("{productId}")]
        public async Task<ActionResult> Edit(Guid productId, [FromForm] ProductEditVM editVM, IFormFile uploadedFile, CancellationToken token)
        {
            if (ModelState == null)
                BadRequest("The invalid model");

            if (!await _productsRepo.IsExist(productId, token))
                return NotFound();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var dto = _mapper.Map<ProductEdit>(editVM);

            try
            {
                await _productsRepo.Edit(productId, dto, token);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id, CancellationToken token)
        {
            if (ModelState == null)
                BadRequest("The invalid model");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                await _productsRepo.Delete(id, token);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }
    }
}