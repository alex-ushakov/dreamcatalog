﻿using DreamCatalog.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace DreamCatalog.WebRest
{
    public static class StartupExtensions
    {
        internal static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DreamCatalogContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DreamCatalogDBConnection"));
            });

            return services;
        }

        internal static IServiceCollection AddSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v0.1", new Info
                {
                    Title = "Dream Catalog - HTTP API",
                    Version = "v0.1",
                    Description = "TODO"
                });
                options.CustomSchemaIds(x => x.FullName);
            });

            return services;
        }
    }
}