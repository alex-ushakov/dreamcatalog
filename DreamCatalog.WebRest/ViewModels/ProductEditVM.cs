﻿using System.ComponentModel.DataAnnotations;

namespace DreamCatalog.WebRest.ViewModels
{
    public class ProductEditVM
    {
        [Required] [MinLength(3)] public string Name { get; set; }
        [Required] public decimal Price { get; set; }
        [Required] public int Quantity { get; set; }
        [Required] public string PhotoPath { get; set; }
    }
}