﻿using System;
using System.Collections.Generic;

namespace DreamCatalog.WebRest.ViewModels
{
    public class CategoryGetVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public CategoryGetVM Parent { get; set; }
        public List<CategoryGetVM> Children { get; set; }

        public List<ProductGetList> Products { get; set; }

        public class ProductGetList
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public int Quantity { get; set; }
            public string PhotoPath { get; set; }
            public long TimeStamp { get; set; }
        }
    }
}