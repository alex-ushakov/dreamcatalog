﻿using AutoMapper;
using DreamCatalog.Core.DTOs;
using DreamCatalog.WebRest.ViewModels;

namespace DreamCatalog.WebRest
{
    public class AutoMapperSettings : Profile
    {
        public AutoMapperSettings()
        {
            CreateMap<CategoryGet, CategoryGetVM>().ReverseMap();

            CreateMap<ProductGetList, ProductGetListVM>().ReverseMap();
            CreateMap<ProductGet, ProductGetVM>().ReverseMap();
            CreateMap<ProductAddVM, ProductAdd>().ReverseMap();
            CreateMap<ProductEditVM, ProductEdit>().ReverseMap();
        }
    }
}