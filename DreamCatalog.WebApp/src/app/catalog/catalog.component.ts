import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { CatalogService } from '../catalog.service';

export interface CategoriesModel {}

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css'],
})
export class CatalogComponent implements OnInit {
  catalogId: string;
  private routeSubscription: Subscription;

  constructor(
    private activateRoute: ActivatedRoute,
    private catalogService: CatalogService,
  ) {
    this.routeSubscription = activateRoute.params.subscribe(params => {
      this.catalogId = params['catalogId'];
    });
  }

  categoriesModels: CategoriesModel[];

  ngOnInit() {
    this.categoriesModels = this.catalogService.getCatagories();
  }
}
