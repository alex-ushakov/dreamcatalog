import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  productId: string;
  private routeSubscription: Subscription;

  private sort: string;
  private timestamp: number;
  private offset: number;
  private amount: number;
  private querySubscription: Subscription;

  constructor(private activateRoute: ActivatedRoute) {
    this.routeSubscription = activateRoute.params.subscribe(params => {
      this.productId = params['productId'];
    });

    this.querySubscription = activateRoute.queryParams.subscribe(
      (queryParam: any) => {
        this.sort = queryParam['sort'];
        this.timestamp = queryParam['timestamp'];
        this.offset = queryParam['offset'];
        this.amount = queryParam['amount'];
      },
    );
  }

  ngOnInit() {}
}
