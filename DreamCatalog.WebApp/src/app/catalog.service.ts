import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const COMMAND_URL = 'http://localhost:13503/api/categories/';

@Injectable({
  providedIn: 'root',
})
export class CatalogService {
  constructor(private http: HttpClient) {}

  getCatagories(): any {
    let result;
    this.http.get(COMMAND_URL).subscribe(data => {
      result = data;
    });
    return result;
  }
}
