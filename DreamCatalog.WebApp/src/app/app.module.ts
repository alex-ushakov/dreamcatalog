import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { MatToolbarModule } from '@angular/material';
import { CdkTreeModule } from '@angular/cdk/tree';

import { AppComponent } from './app.component';
import { CatalogComponent } from './catalog/catalog.component';
import { ProductsComponent } from './products/products.component';
import { ProductDetailsComponent } from './product-details/product-details.component';

const productDetailsRoutes: Routes = [
  { path: ':productId', component: ProductDetailsComponent },
];

const productRoutes: Routes = [
  {
    path: 'products',
    component: ProductsComponent,
    children: productDetailsRoutes,
  },
];

const appRoutes: Routes = [
  { path: '', component: CatalogComponent },
  {
    path: 'catalog/:catalogId',
    component: CatalogComponent,
    children: productRoutes,
  },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  declarations: [
    AppComponent,
    CatalogComponent,
    ProductsComponent,
    ProductDetailsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    MatToolbarModule,
    CdkTreeModule,
  ],
  exports: [MatToolbarModule, CdkTreeModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
